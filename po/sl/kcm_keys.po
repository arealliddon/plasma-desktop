# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-23 00:48+0000\n"
"PO-Revision-Date: 2022-11-13 08:07+0100\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100>=3 && n"
"%100<=4 ? 2 : 3);\n"
"X-Generator: Poedit 3.1.1\n"

#: globalaccelmodel.cpp:113
#, kde-format
msgid "Applications"
msgstr "Programi"

#: globalaccelmodel.cpp:113 globalaccelmodel.cpp:274
#, kde-format
msgid "System Services"
msgstr "Sistemske storitve"

#: globalaccelmodel.cpp:187
#, kde-format
msgctxt ""
"%1 is the name of the component, %2 is the action for which saving failed"
msgid "Error while saving shortcut %1: %2"
msgstr "Napaka pri shranjevanju bližnjice %1: %2"

#: globalaccelmodel.cpp:295
#, kde-format
msgctxt "%1 is the name of an application"
msgid "Error while adding %1, it seems it has no actions."
msgstr "Napaka pri dodajanju %1, kot kaže nima nobene dejavnosti."

#: globalaccelmodel.cpp:338
#, kde-format
msgid "Error while communicating with the global shortcuts service"
msgstr "Napaka pri komunikacij z globalno storitvijo bližnjic"

#: kcm_keys.cpp:50
#, kde-format
msgid "Failed to communicate with global shortcuts daemon"
msgstr "Neuspela komunikacija z demonom globalnih bližnjic"

#: kcm_keys.cpp:224 package/contents/ui/main.qml:129
#: standardshortcutsmodel.cpp:29
#, kde-format
msgid "Common Actions"
msgstr "Skupne dejavnosti"

#: kcm_keys.cpp:230
#, kde-format
msgctxt "%2 is the name of a category inside the 'Common Actions' section"
msgid ""
"Shortcut %1 is already assigned to the common %2 action '%3'.\n"
"Do you want to reassign it?"
msgstr ""
"Bližnjica %1 je že dodeljena običajnemu dejanju %2 '%3'.\n"
"Ali jo želite dodeliti drugam?"

#: kcm_keys.cpp:234
#, kde-format
msgid ""
"Shortcut %1 is already assigned to action '%2' of %3.\n"
"Do you want to reassign it?"
msgstr ""
"Bližnjica %1 je že dodeljena dejanju '%2' od %3.\n"
"Ali jo želite dodeliti drugam?"

#: kcm_keys.cpp:235
#, kde-format
msgctxt "@title:window"
msgid "Found conflict"
msgstr "Najden spor"

#: package/contents/ui/main.qml:41
#, kde-format
msgid "Cannot export scheme while there are unsaved changes"
msgstr "Sheme ni mogoče izvoziti, kadar obstajajo neshranjene spremembe"

#: package/contents/ui/main.qml:53
#, kde-format
msgid ""
"Select the components below that should be included in the exported scheme"
msgstr "Izberite komponente spodaj, ki naj bodo vključene v izvoženo shemo"

#: package/contents/ui/main.qml:59
#, kde-format
msgid "Save scheme"
msgstr "Shrani shemo"

#: package/contents/ui/main.qml:136
#, kde-format
msgid "Remove all shortcuts for %1"
msgstr "Odstrani vse bližnjice za %1"

#: package/contents/ui/main.qml:147
#, kde-format
msgid "Undo deletion"
msgstr "Razveljavi brisanje"

#: package/contents/ui/main.qml:198
#, kde-format
msgid "No items matched the search terms"
msgstr "Ni postavk, ki bi ustrezale kriteriju iskanja"

#: package/contents/ui/main.qml:224
#, kde-format
msgid "Select an item from the list to view its shortcuts here"
msgstr "Izberite predmet s seznama, da si ogledate njegove bližnjice"

#: package/contents/ui/main.qml:232
#, kde-format
msgid "Add Application…"
msgstr "Dodaj program…"

#: package/contents/ui/main.qml:242
#, kde-format
msgid "Import Scheme…"
msgstr "Uvozi shemo…"

#: package/contents/ui/main.qml:247
#, kde-format
msgid "Cancel Export"
msgstr "Prekliči izvoz"

#: package/contents/ui/main.qml:247
#, kde-format
msgid "Export Scheme…"
msgstr "Izvozi shemo…"

#: package/contents/ui/main.qml:268
#, kde-format
msgid "Export Shortcut Scheme"
msgstr "Izvozi shemo bližnjic"

#: package/contents/ui/main.qml:268 package/contents/ui/main.qml:291
#, kde-format
msgid "Import Shortcut Scheme"
msgstr "Uvozi shemo bližnjic"

#: package/contents/ui/main.qml:270
#, kde-format
msgctxt "Template for file dialog"
msgid "Shortcut Scheme (*.kksrc)"
msgstr "Shema bližnjic (*.kksrc)"

#: package/contents/ui/main.qml:296
#, kde-format
msgid "Select the scheme to import:"
msgstr "Izberi shemo za uvoz:"

#: package/contents/ui/main.qml:308
#, kde-format
msgid "Custom Scheme"
msgstr "Shema po meri"

#: package/contents/ui/main.qml:313
#, kde-format
msgid "Select File…"
msgstr "Izberi datoteko…"

#: package/contents/ui/main.qml:313
#, kde-format
msgid "Import"
msgstr "Uvoz"

#: package/contents/ui/ShortcutActionDelegate.qml:26
#, kde-format
msgid "Editing shortcut: %1"
msgstr "Urejanje bližnjice: %1"

#: package/contents/ui/ShortcutActionDelegate.qml:38
#, kde-format
msgctxt ""
"%1 is the name action that is triggered by the key sequences following "
"after :"
msgid "%1:"
msgstr "%1:"

#: package/contents/ui/ShortcutActionDelegate.qml:51
#, kde-format
msgid "No active shortcuts"
msgstr "Ni aktivnih bližnjic"

#: package/contents/ui/ShortcutActionDelegate.qml:90
#, kde-format
msgctxt "%1 decides if singular or plural will be used"
msgid "Default shortcut"
msgid_plural "Default shortcuts"
msgstr[0] "Privzete bližnjica"
msgstr[1] "Privzeti bližnjici"
msgstr[2] "Privzete bližnjice"
msgstr[3] "Privzete bližnjice"

#: package/contents/ui/ShortcutActionDelegate.qml:92
#, kde-format
msgid "No default shortcuts"
msgstr "Ni privzetih bližnjic"

#: package/contents/ui/ShortcutActionDelegate.qml:100
#, kde-format
msgid "Default shortcut %1 is enabled."
msgstr "Privzeta bližnjica %1 je omogočena."

#: package/contents/ui/ShortcutActionDelegate.qml:100
#, kde-format
msgid "Default shortcut %1 is disabled."
msgstr "Privzeta bližnjica %1 je onemogočena."

#: package/contents/ui/ShortcutActionDelegate.qml:121
#, kde-format
msgid "Custom shortcuts"
msgstr "Bližnjice po meri"

#: package/contents/ui/ShortcutActionDelegate.qml:145
#, kde-format
msgid "Delete this shortcut"
msgstr "Izbriši to bližnjico"

#: package/contents/ui/ShortcutActionDelegate.qml:151
#, kde-format
msgid "Add custom shortcut"
msgstr "Dodaj bližnjico po meri"

#: package/contents/ui/ShortcutActionDelegate.qml:185
#, kde-format
msgid "Cancel capturing of new shortcut"
msgstr "Prekliči zajem nove bližnjice"

#: standardshortcutsmodel.cpp:33
#, kde-format
msgid "File"
msgstr "Datoteka"

#: standardshortcutsmodel.cpp:34
#, kde-format
msgid "Edit"
msgstr "Uredi"

#: standardshortcutsmodel.cpp:36
#, kde-format
msgid "Navigation"
msgstr "Krmarjenje"

#: standardshortcutsmodel.cpp:37
#, kde-format
msgid "View"
msgstr "Pogled"

#: standardshortcutsmodel.cpp:38
#, kde-format
msgid "Settings"
msgstr "Nastavitve"

#: standardshortcutsmodel.cpp:39
#, kde-format
msgid "Help"
msgstr "Pomoč"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Matjaž Jeran"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "matjaz.jeran@amis.net"

#~ msgid "Shortcuts"
#~ msgstr "Bližnjice"

#~ msgid "David Redondo"
#~ msgstr "David Redondo"
