# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2010, 2011, 2012, 2013, 2014, 2015, 2016.
# Vit Pelcak <vpelcak@suse.cz>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_trash\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-16 00:49+0000\n"
"PO-Revision-Date: 2021-06-02 16:18+0200\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.04.1\n"

#: contents/ui/main.qml:103
#, kde-format
msgctxt "a verb"
msgid "Open"
msgstr "Otevřít"

#: contents/ui/main.qml:104
#, kde-format
msgctxt "a verb"
msgid "Empty"
msgstr "Vyprázdnit"

#: contents/ui/main.qml:110
#, kde-format
msgid "Trash Settings…"
msgstr "Nastavení koše…"

#: contents/ui/main.qml:161
#, kde-format
msgid ""
"Trash\n"
"Empty"
msgstr ""
"Koš\n"
" Prázdný"

#: contents/ui/main.qml:161
#, kde-format
msgid ""
"Trash\n"
"One item"
msgid_plural ""
"Trash\n"
" %1 items"
msgstr[0] ""
"Koš\n"
"Jedna položka"
msgstr[1] ""
"Koš\n"
" %1 položky"
msgstr[2] ""
"Koš\n"
" %1 položek"

#: contents/ui/main.qml:170
#, kde-format
msgid "Trash"
msgstr "Koš"

#: contents/ui/main.qml:171
#, kde-format
msgid "Empty"
msgstr "Prázdný"

#: contents/ui/main.qml:171
#, kde-format
msgid "One item"
msgid_plural "%1 items"
msgstr[0] "Jedna položka"
msgstr[1] "%1 položky"
msgstr[2] "%1 položky"
