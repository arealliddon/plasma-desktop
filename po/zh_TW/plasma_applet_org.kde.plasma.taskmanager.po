# translation of plasma_applet_tasks.po to Chinese Traditional
# translation of plasma_applet_tasks.po to
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Franklin Weng <franklin@goodhorse.idv.tw>, 2008, 2010, 2011, 2013, 2015.
# Jeff Huang <s8321414@gmail.com>, 2016, 2017.
# pan93412 <pan93412@gmail.com>, 2018, 2019, 2020.
# Franklin Weng <franklin at goodhorse dot idv dot tw>, 2008.
# Frank Weng (a.k.a. Franklin) <franklin at goodhorse dot idv dot tw>, 2008, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_tasks\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-13 00:47+0000\n"
"PO-Revision-Date: 2020-02-20 19:09+0800\n"
"Last-Translator: Yi-Jyun Pan <pan93412@gmail.com>\n"
"Language-Team: Chinese <zh-l10n@lists.linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 19.12.2\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "Appearance"
msgstr "外觀"

#: package/contents/config/config.qml:18
#, kde-format
msgid "Behavior"
msgstr "行為"

#: package/contents/ui/AudioStream.qml:101
#, kde-format
msgctxt "@action:button"
msgid "Unmute"
msgstr ""

#: package/contents/ui/AudioStream.qml:101
#, fuzzy, kde-format
#| msgid "Mute"
msgctxt "@action:button"
msgid "Mute"
msgstr "靜音"

#: package/contents/ui/AudioStream.qml:102
#, fuzzy, kde-format
#| msgid "Mute"
msgctxt "@info:tooltip %1 is the window title"
msgid "Unmute %1"
msgstr "靜音"

#: package/contents/ui/AudioStream.qml:102
#, fuzzy, kde-format
#| msgid "Mute"
msgctxt "@info:tooltip %1 is the window title"
msgid "Mute %1"
msgstr "靜音"

#: package/contents/ui/Badge.qml:54
#, kde-format
msgctxt "Invalid number of new messages, overlay, keep short"
msgid "—"
msgstr "—"

#: package/contents/ui/Badge.qml:56
#, kde-format
msgctxt "Over 9999 new messages, overlay, keep short"
msgid "9,999+"
msgstr "9,999+"

#: package/contents/ui/ConfigAppearance.qml:33
#, kde-format
msgid "General:"
msgstr "一般："

#: package/contents/ui/ConfigAppearance.qml:34
#, fuzzy, kde-format
#| msgid "Highlight windows when hovering over tasks"
msgid "Show small window previews when hovering over Tasks"
msgstr "當游標停留在工作上時突顯視窗"

#: package/contents/ui/ConfigAppearance.qml:43
#, fuzzy, kde-format
#| msgid "Highlight windows when hovering over tasks"
msgid "Hide other windows when hovering over previews"
msgstr "當游標停留在工作上時突顯視窗"

#: package/contents/ui/ConfigAppearance.qml:50
#, kde-format
msgid "Mark applications that play audio"
msgstr "標記播放音訊的應用程式"

#: package/contents/ui/ConfigAppearance.qml:58
#, kde-format
msgctxt "@option:check"
msgid "Fill free space on Panel"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:67
#, kde-format
msgid "Maximum columns:"
msgstr "最大欄數："

#: package/contents/ui/ConfigAppearance.qml:67
#, kde-format
msgid "Maximum rows:"
msgstr "最大列數："

#: package/contents/ui/ConfigAppearance.qml:73
#, kde-format
msgid "Always arrange tasks in rows of as many columns"
msgstr "總是將工作安排在與列數一樣多的行中"

#: package/contents/ui/ConfigAppearance.qml:73
#, kde-format
msgid "Always arrange tasks in columns of as many rows"
msgstr "總是將工作安排在與行數一樣多的列中"

#: package/contents/ui/ConfigAppearance.qml:83
#, kde-format
msgid "Spacing between icons:"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:87
#, fuzzy, kde-format
#| msgid "Small"
msgctxt "@item:inlistbox Icon spacing"
msgid "Small"
msgstr "小"

#: package/contents/ui/ConfigAppearance.qml:91
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Normal"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:95
#, fuzzy, kde-format
#| msgid "Large"
msgctxt "@item:inlistbox Icon spacing"
msgid "Large"
msgstr "大"

#: package/contents/ui/ConfigAppearance.qml:119
#, kde-format
msgctxt "@info:usagetip under a set of radio buttons when Touch Mode is on"
msgid "Automatically set to Large when in Touch Mode"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:47
#, kde-format
msgid "Group:"
msgstr "群組："

#: package/contents/ui/ConfigBehavior.qml:50
#, kde-format
msgid "Do not group"
msgstr "不要分組"

#: package/contents/ui/ConfigBehavior.qml:50
#, kde-format
msgid "By program name"
msgstr "依程式名稱"

#: package/contents/ui/ConfigBehavior.qml:55
#, kde-format
msgid "Clicking grouped task:"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:62
#, fuzzy, kde-format
#| msgid "Cycle through tasks"
msgctxt "Completes the sentence 'Clicking grouped task cycles through tasks' "
msgid "Cycles through tasks"
msgstr "循環切換工作"

#: package/contents/ui/ConfigBehavior.qml:63
#, kde-format
msgctxt ""
"Completes the sentence 'Clicking grouped task shows tooltip window "
"thumbnails' "
msgid "Shows small window previews"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:64
#, kde-format
msgctxt ""
"Completes the sentence 'Clicking grouped task shows windows side by side' "
msgid "Shows large window previews"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:65
#, kde-format
msgctxt "Completes the sentence 'Clicking grouped task shows textual list' "
msgid "Shows textual list"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:73
#, kde-format
msgid ""
"Tooltips are disabled, so the windows will be displayed side by side instead."
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:80
#, kde-format
msgid ""
"Tooltips are disabled, and the compositor does not support displaying "
"windows side by side, so a textual list will be displayed instead"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:87
#, kde-format
msgid ""
"The compositor does not support displaying windows side by side, so a "
"textual list will be displayed instead."
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:97
#, kde-format
msgid "Combine into single button"
msgstr "結合成單一按鈕"

#: package/contents/ui/ConfigBehavior.qml:104
#, kde-format
msgid "Group only when the Task Manager is full"
msgstr "只在工作管理員空間不足時分組"

#: package/contents/ui/ConfigBehavior.qml:115
#, kde-format
msgid "Sort:"
msgstr "排序："

#: package/contents/ui/ConfigBehavior.qml:118
#, kde-format
msgid "Do not sort"
msgstr "請勿排序"

#: package/contents/ui/ConfigBehavior.qml:118
#, kde-format
msgid "Manually"
msgstr "手動"

#: package/contents/ui/ConfigBehavior.qml:118
#, kde-format
msgid "Alphabetically"
msgstr "依字母順序"

#: package/contents/ui/ConfigBehavior.qml:118
#, kde-format
msgid "By desktop"
msgstr "依桌面"

#: package/contents/ui/ConfigBehavior.qml:118
#, kde-format
msgid "By activity"
msgstr "依活動"

#: package/contents/ui/ConfigBehavior.qml:124
#, kde-format
msgid "Keep launchers separate"
msgstr "讓啟動器保持獨立"

#: package/contents/ui/ConfigBehavior.qml:135
#, kde-format
msgctxt "Part of a sentence: 'Clicking active task minimizes the task'"
msgid "Clicking active task:"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:136
#, kde-format
msgctxt "Part of a sentence: 'Clicking active task minimizes the task'"
msgid "Minimizes the task"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:141
#, kde-format
msgid "Middle-clicking any task:"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:145
#, kde-format
msgctxt "Part of a sentence: 'Middle-clicking any task does nothing'"
msgid "Does nothing"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:146
#, fuzzy, kde-format
#| msgid "Close window or group"
msgctxt "Part of a sentence: 'Middle-clicking any task closes window or group'"
msgid "Closes window or group"
msgstr "關閉視窗或群組"

#: package/contents/ui/ConfigBehavior.qml:147
#, fuzzy, kde-format
#| msgid "New instance"
msgctxt "Part of a sentence: 'Middle-clicking any task opens a new window'"
msgid "Opens a new window"
msgstr "新的實體"

#: package/contents/ui/ConfigBehavior.qml:148
#, fuzzy, kde-format
#| msgid "Minimize/Restore window or group"
msgctxt ""
"Part of a sentence: 'Middle-clicking any task minimizes/restores window or "
"group'"
msgid "Minimizes/Restores window or group"
msgstr "最小化/復原視窗"

#: package/contents/ui/ConfigBehavior.qml:149
#, kde-format
msgctxt "Part of a sentence: 'Middle-clicking any task toggles grouping'"
msgid "Toggles grouping"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:150
#, fuzzy, kde-format
#| msgid "Bring to the current virtual desktop"
msgctxt ""
"Part of a sentence: 'Middle-clicking any task brings it to the current "
"virtual desktop'"
msgid "Brings it to the current virtual desktop"
msgstr "移至目前虛擬桌面"

#: package/contents/ui/ConfigBehavior.qml:160
#, fuzzy, kde-format
#| msgid "Mouse wheel:"
msgctxt "Part of a sentence: 'Mouse wheel cycles through tasks'"
msgid "Mouse wheel:"
msgstr "滑鼠滾輪："

#: package/contents/ui/ConfigBehavior.qml:161
#, fuzzy, kde-format
#| msgid "Cycle through tasks"
msgctxt "Part of a sentence: 'Mouse wheel cycles through tasks'"
msgid "Cycles through tasks"
msgstr "循環切換工作"

#: package/contents/ui/ConfigBehavior.qml:170
#, kde-format
msgid "Skip minimized tasks"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:181
#, kde-format
msgid "Show only tasks:"
msgstr "只顯示工作："

#: package/contents/ui/ConfigBehavior.qml:182
#, kde-format
msgid "From current screen"
msgstr "從目前螢幕"

#: package/contents/ui/ConfigBehavior.qml:187
#, kde-format
msgid "From current desktop"
msgstr "從目前桌面"

#: package/contents/ui/ConfigBehavior.qml:192
#, kde-format
msgid "From current activity"
msgstr "從目前活動"

#: package/contents/ui/ConfigBehavior.qml:197
#, kde-format
msgid "That are minimized"
msgstr "已最小化"

#: package/contents/ui/ConfigBehavior.qml:206
#, kde-format
msgid "When panel is hidden:"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:207
#, kde-format
msgid "Unhide when a window wants attention"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:219
#, kde-format
msgid "New tasks appear:"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:221
#: package/contents/ui/ConfigBehavior.qml:229
#, kde-format
msgid "To the right"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:221
#: package/contents/ui/ConfigBehavior.qml:229
#, kde-format
msgid "To the left"
msgstr ""

#: package/contents/ui/ContextMenu.qml:93
#, kde-format
msgid "Places"
msgstr "位置"

#: package/contents/ui/ContextMenu.qml:98
#, fuzzy, kde-format
#| msgid "Recent Documents"
msgid "Recent Files"
msgstr "最近的文件"

#: package/contents/ui/ContextMenu.qml:103
#, kde-format
msgid "Actions"
msgstr "動作"

#: package/contents/ui/ContextMenu.qml:168
#, kde-format
msgctxt "Play previous track"
msgid "Previous Track"
msgstr "前一軌"

#: package/contents/ui/ContextMenu.qml:182
#, kde-format
msgctxt "Pause playback"
msgid "Pause"
msgstr "暫停"

#: package/contents/ui/ContextMenu.qml:182
#, kde-format
msgctxt "Start playback"
msgid "Play"
msgstr "播放"

#: package/contents/ui/ContextMenu.qml:200
#, kde-format
msgctxt "Play next track"
msgid "Next Track"
msgstr "下一軌"

#: package/contents/ui/ContextMenu.qml:211
#, kde-format
msgctxt "Stop playback"
msgid "Stop"
msgstr "停止"

#: package/contents/ui/ContextMenu.qml:231
#, kde-format
msgctxt "Quit media player app"
msgid "Quit"
msgstr "離開"

#: package/contents/ui/ContextMenu.qml:246
#, kde-format
msgctxt "Open or bring to the front window of media player app"
msgid "Restore"
msgstr "回復"

#: package/contents/ui/ContextMenu.qml:272
#, kde-format
msgid "Mute"
msgstr "靜音"

#: package/contents/ui/ContextMenu.qml:283
#, kde-format
msgid "Open New Window"
msgstr ""

#: package/contents/ui/ContextMenu.qml:299
#, kde-format
msgid "Move to &Desktop"
msgstr "移動到桌面(&D)"

#: package/contents/ui/ContextMenu.qml:323
#, kde-format
msgid "Move &To Current Desktop"
msgstr "移到目前桌面(&T)"

#: package/contents/ui/ContextMenu.qml:332
#, kde-format
msgid "&All Desktops"
msgstr "所有的桌面(&A)"

#: package/contents/ui/ContextMenu.qml:346
#, kde-format
msgctxt "1 = number of desktop, 2 = desktop name"
msgid "&%1 %2"
msgstr "%1 %2(&%1)"

#: package/contents/ui/ContextMenu.qml:360
#, kde-format
msgid "&New Desktop"
msgstr "新增桌面(&N)"

#: package/contents/ui/ContextMenu.qml:380
#, kde-format
msgid "Show in &Activities"
msgstr "在活動中顯示(&A)"

#: package/contents/ui/ContextMenu.qml:404
#, kde-format
msgid "Add To Current Activity"
msgstr "新增到目前活動"

#: package/contents/ui/ContextMenu.qml:414
#, kde-format
msgid "All Activities"
msgstr "所有的活動"

#: package/contents/ui/ContextMenu.qml:471
#, fuzzy, kde-format
#| msgid "Move to &Desktop"
msgid "Move to %1"
msgstr "移動到桌面(&D)"

#: package/contents/ui/ContextMenu.qml:498
#: package/contents/ui/ContextMenu.qml:515
#, kde-format
msgid "&Pin to Task Manager"
msgstr "釘選至工作管理員(&P)"

#: package/contents/ui/ContextMenu.qml:567
#, kde-format
msgid "On All Activities"
msgstr "在所有活動上"

#: package/contents/ui/ContextMenu.qml:573
#, kde-format
msgid "On The Current Activity"
msgstr "在目前的活動上"

#: package/contents/ui/ContextMenu.qml:597
#, kde-format
msgid "Unpin from Task Manager"
msgstr "在工作管理員取消釘選"

#: package/contents/ui/ContextMenu.qml:612
#, kde-format
msgid "More"
msgstr ""

#: package/contents/ui/ContextMenu.qml:621
#, kde-format
msgid "&Move"
msgstr "移動(&M)"

#: package/contents/ui/ContextMenu.qml:630
#, kde-format
msgid "Re&size"
msgstr "調整大小(&S)"

#: package/contents/ui/ContextMenu.qml:644
#, kde-format
msgid "Ma&ximize"
msgstr "最大化(&X)"

#: package/contents/ui/ContextMenu.qml:658
#, kde-format
msgid "Mi&nimize"
msgstr "最小化(&N)"

#: package/contents/ui/ContextMenu.qml:668
#, kde-format
msgid "Keep &Above Others"
msgstr "維持在其它項目之上(&A)"

#: package/contents/ui/ContextMenu.qml:678
#, kde-format
msgid "Keep &Below Others"
msgstr "維持在其它項目之下(&B)"

#: package/contents/ui/ContextMenu.qml:690
#, kde-format
msgid "&Fullscreen"
msgstr "全螢幕(&F)"

#: package/contents/ui/ContextMenu.qml:702
#, kde-format
msgid "&Shade"
msgstr "隱藏(&S)"

#: package/contents/ui/ContextMenu.qml:718
#, kde-format
msgid "Allow this program to be grouped"
msgstr "允許此程式被歸類"

#: package/contents/ui/ContextMenu.qml:766
#, kde-format
msgid "&Close"
msgstr "關閉(&C)"

#: package/contents/ui/Task.qml:72
#, kde-format
msgctxt "@info:usagetip %1 application name"
msgid "Launch %1"
msgstr ""

#: package/contents/ui/Task.qml:77
#, kde-format
msgctxt "@info:tooltip"
msgid "There is %1 new message."
msgid_plural "There are %1 new messages."
msgstr[0] ""

#: package/contents/ui/Task.qml:86
#, fuzzy, kde-format
#| msgid "Show tooltips"
msgctxt "@info:usagetip %1 task name"
msgid "Show Task tooltip for %1"
msgstr "顯示工具提示"

#: package/contents/ui/Task.qml:92
#, kde-format
msgctxt "@info:usagetip %1 task name"
msgid "Show windows side by side for %1"
msgstr ""

#: package/contents/ui/Task.qml:97
#, kde-format
msgctxt "@info:usagetip %1 task name"
msgid "Open textual list of windows for %1"
msgstr ""

#: package/contents/ui/Task.qml:101
#, kde-format
msgid "Activate %1"
msgstr ""

#: package/contents/ui/ToolTipInstance.qml:326
#, kde-format
msgctxt "button to unmute app"
msgid "Unmute %1"
msgstr ""

#: package/contents/ui/ToolTipInstance.qml:327
#, fuzzy, kde-format
#| msgid "Mute"
msgctxt "button to mute app"
msgid "Mute %1"
msgstr "靜音"

#: package/contents/ui/ToolTipInstance.qml:350
#, kde-format
msgctxt "Accessibility data on volume slider"
msgid "Adjust volume for %1"
msgstr ""

#: package/contents/ui/ToolTipInstance.qml:366
#, fuzzy, kde-format
#| msgctxt "1 = number of desktop, 2 = desktop name"
#| msgid "&%1 %2"
msgctxt "volume percentage"
msgid "%1%"
msgstr "%1 %2(&%1)"

#: package/contents/ui/ToolTipInstance.qml:369
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr ""

#: package/contents/ui/ToolTipInstance.qml:393
#, kde-format
msgctxt "Comma-separated list of desktops"
msgid "On %1"
msgstr "於 %1"

#: package/contents/ui/ToolTipInstance.qml:396
#, kde-format
msgctxt "Comma-separated list of desktops"
msgid "Pinned to all desktops"
msgstr ""

#: package/contents/ui/ToolTipInstance.qml:407
#, kde-format
msgctxt "Which virtual desktop a window is currently on"
msgid "Available on all activities"
msgstr "所有活動都可使用"

#: package/contents/ui/ToolTipInstance.qml:429
#, kde-format
msgctxt "Activities a window is currently on (apart from the current one)"
msgid "Also available on %1"
msgstr "也可在 %1 上使用"

#: package/contents/ui/ToolTipInstance.qml:433
#, kde-format
msgctxt "Which activities a window is currently on"
msgid "Available on %1"
msgstr "於 %1 上使用"

#: plugin/backend.cpp:326
#, kde-format
msgctxt "Show all user Places"
msgid "%1 more Place"
msgid_plural "%1 more Places"
msgstr[0] "%1 個更多位置"

#: plugin/backend.cpp:422
#, fuzzy, kde-format
#| msgid "Recent Documents"
msgid "Recent Downloads"
msgstr "最近的文件"

#: plugin/backend.cpp:424
#, fuzzy, kde-format
#| msgid "Recent Documents"
msgid "Recent Connections"
msgstr "最近的文件"

#: plugin/backend.cpp:426
#, fuzzy, kde-format
#| msgid "Recent Documents"
msgid "Recent Places"
msgstr "最近的文件"

#: plugin/backend.cpp:435
#, fuzzy, kde-format
#| msgid "Forget Recent Documents"
msgctxt "@action:inmenu"
msgid "Forget Recent Downloads"
msgstr "忘掉最近的文件"

#: plugin/backend.cpp:437
#, fuzzy, kde-format
#| msgid "Forget Recent Documents"
msgctxt "@action:inmenu"
msgid "Forget Recent Connections"
msgstr "忘掉最近的文件"

#: plugin/backend.cpp:439
#, fuzzy, kde-format
#| msgid "Forget Recent Documents"
msgctxt "@action:inmenu"
msgid "Forget Recent Places"
msgstr "忘掉最近的文件"

#: plugin/backend.cpp:441
#, fuzzy, kde-format
#| msgid "Forget Recent Documents"
msgctxt "@action:inmenu"
msgid "Forget Recent Files"
msgstr "忘掉最近的文件"

#~ msgid "Show tooltips"
#~ msgstr "顯示工具提示"

#~ msgid "Icon size:"
#~ msgstr "圖示大小："

#~ msgid "Start New Instance"
#~ msgstr "開啟新的實體"

#~ msgid "More Actions"
#~ msgstr "更多動作"

#~ msgid "Cycle through tasks"
#~ msgstr "循環切換工作"

#~ msgid "On middle-click:"
#~ msgstr "點擊滑鼠中鍵："

#~ msgctxt "The click action"
#~ msgid "None"
#~ msgstr "無"

#~ msgctxt "When clicking it would toggle grouping windows of a specific app"
#~ msgid "Group/Ungroup"
#~ msgstr "分組／取消分組"

#~ msgid "Open groups in popups"
#~ msgstr "在彈出式視窗中開啟群組"

#~ msgid "Filter:"
#~ msgstr "過濾器："

#~ msgid "Show only tasks from the current desktop"
#~ msgstr "只顯示目前桌面上的工作"

#~ msgid "Show only tasks from the current activity"
#~ msgstr "只顯示目前活動裡的工作"

#~ msgid "Always arrange tasks in as many rows as columns"
#~ msgstr "總是將任務安排在與列數一樣多的欄中"
