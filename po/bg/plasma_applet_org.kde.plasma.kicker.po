# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Mincho Kondarev <mkondarev@yahoo.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-30 00:47+0000\n"
"PO-Revision-Date: 2022-08-17 10:35+0200\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.3\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Общи"

#: package/contents/ui/code/tools.js:42
#, kde-format
msgid "Remove from Favorites"
msgstr "Премахване от Предпочитани"

#: package/contents/ui/code/tools.js:46
#, kde-format
msgid "Add to Favorites"
msgstr "Добавяне в Предпочитани"

#: package/contents/ui/code/tools.js:70
#, kde-format
msgid "On All Activities"
msgstr "На всички дейности"

#: package/contents/ui/code/tools.js:120
#, kde-format
msgid "On the Current Activity"
msgstr "На текущата дейност"

#: package/contents/ui/code/tools.js:134
#, kde-format
msgid "Show in Favorites"
msgstr "Показване в Предпочитани"

#: package/contents/ui/ConfigGeneral.qml:46
#, kde-format
msgid "Icon:"
msgstr "Икона:"

#: package/contents/ui/ConfigGeneral.qml:126
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Избиране…"

#: package/contents/ui/ConfigGeneral.qml:131
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Clear Icon"
msgstr "Изчистване на икона"

#: package/contents/ui/ConfigGeneral.qml:149
#, kde-format
msgid "Show applications as:"
msgstr "Показване на приложения като:"

#: package/contents/ui/ConfigGeneral.qml:151
#, kde-format
msgid "Name only"
msgstr "Само име"

#: package/contents/ui/ConfigGeneral.qml:151
#, kde-format
msgid "Description only"
msgstr "Само описание"

#: package/contents/ui/ConfigGeneral.qml:151
#, kde-format
msgid "Name (Description)"
msgstr "Име (описание)"

#: package/contents/ui/ConfigGeneral.qml:151
#, kde-format
msgid "Description (Name)"
msgstr "Описание (име)"

#: package/contents/ui/ConfigGeneral.qml:161
#, kde-format
msgid "Behavior:"
msgstr "Поведение:"

#: package/contents/ui/ConfigGeneral.qml:163
#, kde-format
msgid "Sort applications alphabetically"
msgstr "Сортиране на приложенията по азбучен ред"

#: package/contents/ui/ConfigGeneral.qml:171
#, kde-format
msgid "Flatten sub-menus to a single level"
msgstr "Изравняване на подменютата до едно ниво"

#: package/contents/ui/ConfigGeneral.qml:179
#, kde-format
msgid "Show icons on the root level of the menu"
msgstr "Показване на икони на основното ниво на менюто"

#: package/contents/ui/ConfigGeneral.qml:189
#, kde-format
msgid "Show categories:"
msgstr "Показване на категории:"

#: package/contents/ui/ConfigGeneral.qml:192
#, kde-format
msgid "Recent applications"
msgstr "Скорошни приложения"

#: package/contents/ui/ConfigGeneral.qml:193
#, kde-format
msgid "Often used applications"
msgstr "Често използвани приложения"

#: package/contents/ui/ConfigGeneral.qml:200
#, kde-format
msgid "Recent files"
msgstr "Скоро отваряни файлове"

#: package/contents/ui/ConfigGeneral.qml:201
#, kde-format
msgid "Often used files"
msgstr "Често използвани файлове"

#: package/contents/ui/ConfigGeneral.qml:208
#, kde-format
msgid "Recent contacts"
msgstr "Скорошни контакти"

#: package/contents/ui/ConfigGeneral.qml:209
#, kde-format
msgid "Often used contacts"
msgstr "Често използвани контакти"

#: package/contents/ui/ConfigGeneral.qml:215
#, kde-format
msgid "Sort items in categories by:"
msgstr "Сортиране на артикулите в категории по:"

#: package/contents/ui/ConfigGeneral.qml:216
#, kde-format
msgctxt ""
"@item:inlistbox Sort items in categories by [Recently used | Often used]"
msgid "Recently used"
msgstr "Наскоро използвани"

#: package/contents/ui/ConfigGeneral.qml:216
#, kde-format
msgctxt ""
"@item:inlistbox Sort items in categories by [Recently used | Ofetn used]"
msgid "Often used"
msgstr "Често използван"

#: package/contents/ui/ConfigGeneral.qml:226
#, kde-format
msgid "Search:"
msgstr "Търсене:"

#: package/contents/ui/ConfigGeneral.qml:228
#, kde-format
msgid "Expand search to bookmarks, files and emails"
msgstr "Разширяване на търсенето до отметки, файлове и имейли"

#: package/contents/ui/ConfigGeneral.qml:236
#, kde-format
msgid "Align search results to bottom"
msgstr "Подравняване на резултатите от търсенето отдолу"

#: package/contents/ui/DashboardRepresentation.qml:292
#, kde-format
msgid "Searching for '%1'"
msgstr "Търсене за \"%1\""

#: package/contents/ui/DashboardRepresentation.qml:292
#, kde-format
msgid "Type to search…"
msgstr "Въведете за търсене…"

#: package/contents/ui/DashboardRepresentation.qml:392
#, kde-format
msgid "Favorites"
msgstr "Предпочитани"

#: package/contents/ui/DashboardRepresentation.qml:610
#: package/contents/ui/DashboardTabBar.qml:42
#, kde-format
msgid "Widgets"
msgstr "Уиджети"

#: package/contents/ui/DashboardTabBar.qml:31
#, kde-format
msgid "Apps & Docs"
msgstr "Приложения и документи"

#: package/contents/ui/main.qml:259
#, kde-format
msgid "Edit Applications…"
msgstr "Редактиране на приложения…"
