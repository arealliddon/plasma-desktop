# translation of kcmlaunch.po to Greek
# Copyright (C) 2003, 2007 Free Software Foundation, Inc.
#
# Dimitris Kamenopoulos <d.kamenopoulos@mail.ntua.gr>, 2002.
# Stergios Dramis <sdramis@egnatia.ee.auth.gr>, 2003.
# Spiros Georgaras <sngeorgaras@otenet.gr>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: kcmlaunch\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-13 00:47+0000\n"
"PO-Revision-Date: 2007-03-18 13:09+0200\n"
"Last-Translator: Spiros Georgaras <sngeorgaras@otenet.gr>\n"
"Language-Team: Greek <i18ngr@lists.hellug.gr>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. i18n: ectx: label, entry (cursorTimeout), group (BusyCursorSettings)
#. i18n: ectx: label, entry (taskbarTimeout), group (TaskbarButtonSettings)
#: launchfeedbacksettingsbase.kcfg:15 launchfeedbacksettingsbase.kcfg:29
#, kde-format
msgid "Timeout in seconds"
msgstr ""

#: package/contents/ui/main.qml:17
#, kde-format
msgid "Launch Feedback"
msgstr ""

#: package/contents/ui/main.qml:33
#, fuzzy, kde-format
#| msgid "Bus&y Cursor"
msgid "Cursor:"
msgstr "&Απασχολημένος δρομέας"

#: package/contents/ui/main.qml:34
#, kde-format
msgid "No Feedback"
msgstr ""

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Static"
msgstr ""

#: package/contents/ui/main.qml:64
#, fuzzy, kde-format
#| msgid "Blinking Cursor"
msgid "Blinking"
msgstr "Απασχολημένος δρομέας που αναβοσβήνει"

#: package/contents/ui/main.qml:79
#, fuzzy, kde-format
#| msgid "Bouncing Cursor"
msgid "Bouncing"
msgstr "Απασχολημένος δρομέας που αναπηδά"

#: package/contents/ui/main.qml:97
#, kde-format
msgid "Task Manager:"
msgstr ""

#: package/contents/ui/main.qml:99
#, fuzzy, kde-format
#| msgid "Enable &taskbar notification"
msgid "Enable animation"
msgstr "Ενεργοποίηση ειδοποίησης μέσω &Γραμμής εργασιών"

#: package/contents/ui/main.qml:113
#, fuzzy, kde-format
#| msgid "&Startup indication timeout:"
msgid "Stop animation after:"
msgstr "Χρονικό όριο ένδειξης &εκκίνησης:"

#: package/contents/ui/main.qml:126 package/contents/ui/main.qml:138
#, fuzzy, kde-format
#| msgid " sec"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] " δευτ"
msgstr[1] " δευτ"

#~ msgid ""
#~ "<h1>Launch Feedback</h1> You can configure the application-launch "
#~ "feedback here."
#~ msgstr ""
#~ "<h1>Ειδοποίηση εκκίνησης</h1> Εδώ μπορείτε να ρυθμίσετε την ειδοποίηση "
#~ "εκκίνησης των εφαρμογών."

#~ msgid ""
#~ "<h1>Busy Cursor</h1>\n"
#~ "KDE offers a busy cursor for application startup notification.\n"
#~ "To enable the busy cursor, select one kind of visual feedback\n"
#~ "from the combobox.\n"
#~ "It may occur, that some applications are not aware of this startup\n"
#~ "notification. In this case, the cursor stops blinking after the time\n"
#~ "given in the section 'Startup indication timeout'"
#~ msgstr ""
#~ "<h1>Απασχολημένος δρομέας</h1>\n"
#~ "Το KDE προσφέρει έναν απασχολημένο δρομέα για να\n"
#~ "ειδοποιήστε σχετικά με την εκκίνηση των εφαρμογών.\n"
#~ "Για να τον ενεργοποιήσετε, επιλέξτε έναν τύπο οπτικής\n"
#~ "ειδοποίησης από το πλαίσιο συνδυασμών.\n"
#~ "Ίσως μερικές εφαρμογές να μη χρησιμοποιούν αυτή την υπηρεσία.\n"
#~ "Σε αυτή την περίπτωση, ο δρομέας σταματά να αναβοσβήνει μετά το\n"
#~ "πέρας της διορίας που θα δώσετε στο 'Χρονικό όριο ένδειξης εκκίνησης'"

#~ msgid "No Busy Cursor"
#~ msgstr "Χωρίς απασχολημένο δρομέα"

#~ msgid "Passive Busy Cursor"
#~ msgstr "Παθητικός απασχολημένος δρομέας"

#~ msgid "Taskbar &Notification"
#~ msgstr "&Ειδοποίηση μέσω Γραμμής εργασιών"

#~ msgid ""
#~ "<H1>Taskbar Notification</H1>\n"
#~ "You can enable a second method of startup notification which is\n"
#~ "used by the taskbar where a button with a rotating hourglass appears,\n"
#~ "symbolizing that your started application is loading.\n"
#~ "It may occur, that some applications are not aware of this startup\n"
#~ "notification. In this case, the button disappears after the time\n"
#~ "given in the section 'Startup indication timeout'"
#~ msgstr ""
#~ "<H1>Ειδοποίηση μέσω Γραμμής εργασιών</H1>\n"
#~ "Μπορείτε να ενεργοποιήσετε και μια δεύτερη μέθοδο ειδοποίησης για την\n"
#~ "εκκίνηση μιας εφαρμογής η οποία χρησιμοποιείται από τη Γραμμή εργασιών,\n"
#~ "όπου εμφανίζεται ένα κουμπί με μια κλεψύδρα να γυρίζει, φανερώνοντας\n"
#~ "ότι η αντίστοιχη εφαρμογή ακόμα φορτώνει. Ίσως μερικές εφαρμογές να μη\n"
#~ "χρησιμοποιούν αυτή την υπηρεσία. Σε αυτή την περίπτωση, το κουμπί\n"
#~ "εξαφανίζεται μετά το πέρας της διορίας που θα δώσετε στο 'Χρονικό όριο\n"
#~ "ένδειξης εκκίνησης'"

#~ msgid "Start&up indication timeout:"
#~ msgstr "Χρονικό όριο ένδειξης ε&κκίνησης:"
