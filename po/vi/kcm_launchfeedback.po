# Vietnamese translation for kcmlaunch.
# Copyright © 2006 Free Software Foundation, Inc.
#
# Nguyễn Hưng Vũ <Vu.Hung@techviet.com>, 2002.
# Phan Vĩnh Thịnh <teppi82@gmail.com>, 2006.
# Phu Hung Nguyen <phu.nguyen@kdemail.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kcmlaunch\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-13 00:47+0000\n"
"PO-Revision-Date: 2021-05-27 09:03+0200\n"
"Last-Translator: Phu Hung Nguyen <phu.nguyen@kdemail.net>\n"
"Language-Team: Vietnamese <kde-l10n-vi@kde.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 20.08.1\n"

#. i18n: ectx: label, entry (cursorTimeout), group (BusyCursorSettings)
#. i18n: ectx: label, entry (taskbarTimeout), group (TaskbarButtonSettings)
#: launchfeedbacksettingsbase.kcfg:15 launchfeedbacksettingsbase.kcfg:29
#, kde-format
msgid "Timeout in seconds"
msgstr "Khoảng thời gian tính bằng giây"

#: package/contents/ui/main.qml:17
#, kde-format
msgid "Launch Feedback"
msgstr "Phản hồi khởi chạy"

#: package/contents/ui/main.qml:33
#, kde-format
msgid "Cursor:"
msgstr "Con trỏ:"

#: package/contents/ui/main.qml:34
#, kde-format
msgid "No Feedback"
msgstr "Không phản hồi"

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Static"
msgstr "Tĩnh"

#: package/contents/ui/main.qml:64
#, kde-format
msgid "Blinking"
msgstr "Nhấp nháy"

#: package/contents/ui/main.qml:79
#, kde-format
msgid "Bouncing"
msgstr "Nảy"

#: package/contents/ui/main.qml:97
#, kde-format
msgid "Task Manager:"
msgstr "Trình quản lí tác vụ:"

#: package/contents/ui/main.qml:99
#, kde-format
msgid "Enable animation"
msgstr "Bật hiệu ứng động"

#: package/contents/ui/main.qml:113
#, kde-format
msgid "Stop animation after:"
msgstr "Dừng hiệu ứng động sau:"

#: package/contents/ui/main.qml:126 package/contents/ui/main.qml:138
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 giây"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Nguyễn Hùng Phú"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "phu.nguyen@kdemail.net"

#~ msgid "Configure application launch feedback"
#~ msgstr "Cấu hình phản hồi khởi chạy ứng dụng"
