# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Safa Alfulaij <safa1996alfulaij@gmail.com>, 2014, 2017.
# Zayed Al-Saidi <zayed.alsaidi@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-16 00:49+0000\n"
"PO-Revision-Date: 2021-06-19 14:04+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 19.12.3\n"

#: contents/ui/main.qml:103
#, kde-format
msgctxt "a verb"
msgid "Open"
msgstr "افتح"

#: contents/ui/main.qml:104
#, kde-format
msgctxt "a verb"
msgid "Empty"
msgstr "أفرغ"

#: contents/ui/main.qml:110
#, kde-format
msgid "Trash Settings…"
msgstr "إعدادات المهملات..."

#: contents/ui/main.qml:161
#, kde-format
msgid ""
"Trash\n"
"Empty"
msgstr ""
"المهملات\n"
"فارغة"

#: contents/ui/main.qml:161
#, kde-format
msgid ""
"Trash\n"
"One item"
msgid_plural ""
"Trash\n"
" %1 items"
msgstr[0] ""
"المهملات\n"
"لا عناصر"
msgstr[1] ""
"المهملات\n"
" عنصر واحد"
msgstr[2] ""
"المهملات\n"
" عنصران"
msgstr[3] ""
"المهملات\n"
" %1 عناصر"
msgstr[4] ""
"المهملات\n"
" %1 عنصرًا"
msgstr[5] ""
"المهملات\n"
" %1 عنصر"

#: contents/ui/main.qml:170
#, kde-format
msgid "Trash"
msgstr "المهملات"

#: contents/ui/main.qml:171
#, kde-format
msgid "Empty"
msgstr "فارغة"

#: contents/ui/main.qml:171
#, kde-format
msgid "One item"
msgid_plural "%1 items"
msgstr[0] "لا عناصر"
msgstr[1] "عنصر واحد"
msgstr[2] "عنصران"
msgstr[3] "%1 عناصر"
msgstr[4] "%1 عنصرًا"
msgstr[5] "%1 عنصر"

#~ msgid ""
#~ "Trash \n"
#~ " Empty"
#~ msgstr ""
#~ "المهملات \n"
#~ " فارغة"

#~ msgid "Empty Trash"
#~ msgstr "أفرغ المهملات"

#~ msgid ""
#~ "Do you really want to empty the trash ? All the items will be deleted."
#~ msgstr "أتريد حقًّا إفراغ المهملات؟ ستُحذف كل العناصر."

#~ msgid "Cancel"
#~ msgstr "ألغِ"
