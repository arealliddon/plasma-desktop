# translation of kcmsmserver.po to Тоҷикӣ
# Copyright (C) 2004 Free Software Foundation, Inc.
#
# Victor Ibragimov <victor.ibragimov@gmail.com>, 2004, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kcmsmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-14 00:48+0000\n"
"PO-Revision-Date: 2019-09-16 18:09+0500\n"
"Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>\n"
"Language-Team: English <kde-i18n-doc@kde.org>\n"
"Language: tg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 19.04.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: kcmsmserver.cpp:49
#, kde-format
msgid ""
"<h1>Session Manager</h1> You can configure the session manager here. This "
"includes options such as whether or not the session exit (logout) should be "
"confirmed, whether the session should be restored again when logging in and "
"whether the computer should be automatically shut down after session exit by "
"default."
msgstr ""

#: package/contents/ui/main.qml:25
#, kde-format
msgid "Failed to request restart to firmware setup: %1"
msgstr ""

#: package/contents/ui/main.qml:26
#, kde-format
msgid ""
"Next time the computer is restarted, it will enter the UEFI setup screen."
msgstr ""

#: package/contents/ui/main.qml:27
#, kde-format
msgid ""
"Next time the computer is restarted, it will enter the firmware setup screen."
msgstr ""

#: package/contents/ui/main.qml:32
#, kde-format
msgid "Restart Now"
msgstr "Ҳозир аз нав оғоз карда шавад"

#: package/contents/ui/main.qml:38
#, fuzzy, kde-format
#| msgid "General"
msgid "General:"
msgstr "Умумӣ"

#: package/contents/ui/main.qml:39
#, kde-format
msgctxt "@option:check"
msgid "Confirm logout"
msgstr ""

#: package/contents/ui/main.qml:48
#, kde-format
msgctxt "@option:check"
msgid "Offer shutdown options"
msgstr ""

#: package/contents/ui/main.qml:65
#, kde-format
msgid "Default leave option:"
msgstr ""

#: package/contents/ui/main.qml:66
#, kde-format
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "End current session"
msgstr ""

#: package/contents/ui/main.qml:76
#, fuzzy, kde-format
#| msgid "&Restart computer"
msgctxt "@option:radio"
msgid "Restart computer"
msgstr "&Аз нав оғоз кардани компютер"

#: package/contents/ui/main.qml:86
#, fuzzy, kde-format
#| msgid "&Turn off computer"
msgctxt "@option:radio"
msgid "Turn off computer"
msgstr "&Хомӯш кардани компютер"

#: package/contents/ui/main.qml:102
#, kde-format
msgid "When logging in:"
msgstr ""

#: package/contents/ui/main.qml:103
#, kde-format
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "Restore last session"
msgstr ""

#: package/contents/ui/main.qml:113
#, kde-format
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "Restore last manually saved session"
msgstr ""

#: package/contents/ui/main.qml:123
#, kde-format
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "Start with an empty session"
msgstr ""

#: package/contents/ui/main.qml:135
#, kde-format
msgid "Don't restore these applications:"
msgstr ""

#: package/contents/ui/main.qml:152
#, kde-format
msgid ""
"Here you can enter a colon or comma separated list of applications that "
"should not be saved in sessions, and therefore will not be started when "
"restoring a session. For example 'xterm:konsole' or 'xterm,konsole'."
msgstr ""

#: package/contents/ui/main.qml:161
#, kde-format
msgctxt "@option:check"
msgid "Enter UEFI setup screen on next restart"
msgstr ""

#: package/contents/ui/main.qml:162
#, kde-format
msgctxt "@option:check"
msgid "Enter firmware setup screen on next restart"
msgstr ""

#. i18n: ectx: label, entry (confirmLogout), group (General)
#: smserversettings.kcfg:9
#, kde-format
msgid "Confirm logout"
msgstr ""

#. i18n: ectx: label, entry (offerShutdown), group (General)
#: smserversettings.kcfg:13
#, kde-format
msgid "Offer shutdown options"
msgstr ""

#. i18n: ectx: label, entry (shutdownType), group (General)
#: smserversettings.kcfg:17
#, kde-format
msgid "Default leave option"
msgstr ""

#. i18n: ectx: label, entry (loginMode), group (General)
#: smserversettings.kcfg:26
#, kde-format
msgid "On login"
msgstr ""

#. i18n: ectx: label, entry (excludeApps), group (General)
#: smserversettings.kcfg:30
#, kde-format
msgid "Applications to be excluded from session"
msgstr ""

#~ msgid "Session Manager"
#~ msgstr "Мудири сеанс"

#~ msgid "Advanced"
#~ msgstr "Интихоботҳои иловагӣ"
