# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Sveinn í Felli <sv1@fellsnet.is>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-20 00:47+0000\n"
"PO-Revision-Date: 2022-09-09 13:29+0000\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>\n"
"Language-Team: Icelandic\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "Appearance"
msgstr "Útlit"

#: package/contents/ui/ConfigAppearance.qml:24
#, kde-format
msgid "Icon:"
msgstr "Táknmynd:"

#: package/contents/ui/ConfigAppearance.qml:26
#, kde-format
msgid "Show the current activity icon"
msgstr "Sýna táknmynd fyrir núverandi athöfn"

#: package/contents/ui/ConfigAppearance.qml:32
#, kde-format
msgid "Show the generic activity icon"
msgstr "Sýna táknmynd fyrir almenna athöfn"

#: package/contents/ui/ConfigAppearance.qml:42
#, kde-format
msgid "Title:"
msgstr "Titill:"

#: package/contents/ui/ConfigAppearance.qml:44
#, kde-format
msgid "Show the current activity name"
msgstr "Sýna heiti á núverandi athöfn"

#: package/contents/ui/main.qml:73
#, kde-format
msgctxt "@info:tooltip"
msgid "Current activity is %1"
msgstr "Núverandi athöfn er %1"

#: package/contents/ui/main.qml:83
#, kde-format
msgid "Show Activity Manager"
msgstr "Birta Athafnastjóra"

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Click to show the activity manager"
msgstr "Smelltu til að birta athafnastjórann"
