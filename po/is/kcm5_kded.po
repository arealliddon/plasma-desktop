# translation of kcmkded.po to icelandic
# Copyright (C) 2003, 2008 Free Software Foundation, Inc.
# Pjetur G. Hjaltason <pjetur@pjetur.net>, 2003.
# Sveinn í Felli <sveinki@nett.is>, 2008, 2015.
msgid ""
msgstr ""
"Project-Id-Version: kcmkded\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-04 00:47+0000\n"
"PO-Revision-Date: 2015-10-22 09:20+0000\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>\n"
"Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"\n"
"\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Richard Allen, Pjetur G. Hjaltason"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ra@ra.is, pjetur@pjetur.net"

#: kcmkded.cpp:48
#, fuzzy, kde-format
#| msgid "Startup Services"
msgid "Background Services"
msgstr "Ræsi þjónustur"

#: kcmkded.cpp:52
#, fuzzy, kde-format
#| msgid "(c) 2002 Daniel Molkentin"
msgid "(c) 2002 Daniel Molkentin, (c) 2020 Kai Uwe Broulik"
msgstr "(c) 2002 Daniel Molkentin"

#: kcmkded.cpp:53
#, kde-format
msgid "Daniel Molkentin"
msgstr "Daniel Molkentin"

#: kcmkded.cpp:54
#, kde-format
msgid "Kai Uwe Broulik"
msgstr ""

#: kcmkded.cpp:125
#, fuzzy, kde-format
#| msgid "Unable to stop service <em>%1</em>."
msgid "Failed to stop service: %1"
msgstr "Get ekki stöðvað þjónustu <em>%1</em>."

#: kcmkded.cpp:127
#, fuzzy, kde-format
#| msgid "Unable to start server <em>%1</em>."
msgid "Failed to start service: %1"
msgstr "Get ekki ræst þjónustuna <em>%1</em>."

#: kcmkded.cpp:134
#, fuzzy, kde-format
#| msgid "Unable to stop service <em>%1</em>."
msgid "Failed to stop service."
msgstr "Get ekki stöðvað þjónustu <em>%1</em>."

#: kcmkded.cpp:136
#, fuzzy, kde-format
#| msgid "Unable to start server <em>%1</em>."
msgid "Failed to start service."
msgstr "Get ekki ræst þjónustuna <em>%1</em>."

#: kcmkded.cpp:234
#, kde-format
msgid "Failed to notify KDE Service Manager (kded5) of saved changed: %1"
msgstr ""

#: package/contents/ui/main.qml:19
#, fuzzy, kde-format
#| msgid ""
#| "<h1>Service Manager</h1><p>This module allows you to have an overview of "
#| "all plugins of the KDE Daemon, also referred to as KDE Services. "
#| "Generally, there are two types of service:</p><ul><li>Services invoked at "
#| "startup</li><li>Services called on demand</li></ul><p>The latter are only "
#| "listed for convenience. The startup services can be started and stopped. "
#| "In Administrator mode, you can also define whether services should be "
#| "loaded at startup.</p><p><b> Use this with care: some services are vital "
#| "for KDE; do not deactivate services if you do not know what you are doing."
#| "</b></p>"
msgid ""
"<p>This module allows you to have an overview of all plugins of the KDE "
"Daemon, also referred to as KDE Services. Generally, there are two types of "
"service:</p> <ul><li>Services invoked at startup</li><li>Services called on "
"demand</li></ul> <p>The latter are only listed for convenience. The startup "
"services can be started and stopped. You can also define whether services "
"should be loaded at startup.</p> <p><b>Use this with care: some services are "
"vital for Plasma; do not deactivate services if you  do not know what you "
"are doing.</b></p>"
msgstr ""
"<h1>KDE þjónustur</h1> <p>Þessi eining sýnir þér yfirlit yfir allar viðbætur "
"KDE þjónsins, einnig nefnt KDE þjónustur. Almennt talað eru tvær tegundir "
"þjónusta:</p> <ul> <li>Þjónustur gangsettar í ræsingu</li> <li>Þjónustur "
"gangsettar eftir þörfum</li></ul> <p>Þjónustur gangsettar eftir þörfum eru "
"hér aðeins sýndar hér þér til þæginda. Þjónustur sem gangsettar eru í "
"ræsingu má stöðva og ræsa. Í kerfisstjóraham getur þú einnig silgreint hvort "
"þær skuli ræstar í byrjun.</p> <p><b> Notið þetta með varúð.  Sumar "
"þjónustur eru lífsnauðsynlegar fyrir KDE. stöðvið ekki þjónustur nema þið "
"vitið nákvæmlega hvað þið eruð að gera.</b></p>"

#: package/contents/ui/main.qml:38
#, kde-format
msgid ""
"The background services manager (kded5) is currently not running. Make sure "
"it is installed correctly."
msgstr ""

#: package/contents/ui/main.qml:47
#, kde-format
msgid ""
"Some services disable themselves again when manually started if they are not "
"useful in the current environment."
msgstr ""

#: package/contents/ui/main.qml:56
#, kde-format
msgid ""
"Some services were automatically started/stopped when the background "
"services manager (kded5) was restarted to apply your changes."
msgstr ""

#: package/contents/ui/main.qml:98
#, fuzzy, kde-format
#| msgid "Service"
msgid "All Services"
msgstr "Þjónusta"

#: package/contents/ui/main.qml:99
#, fuzzy, kde-format
#| msgid "Running"
msgctxt "List running services"
msgid "Running"
msgstr "Keyrandi"

#: package/contents/ui/main.qml:100
#, fuzzy, kde-format
#| msgid "Not running"
msgctxt "List not running services"
msgid "Not Running"
msgstr "Ekki keyrandi"

#: package/contents/ui/main.qml:136
#, fuzzy, kde-format
#| msgid "Startup Services"
msgid "Startup Services"
msgstr "Ræsi þjónustur"

#: package/contents/ui/main.qml:137
#, kde-format
msgid "Load-on-Demand Services"
msgstr "Þjónustur sem ræstar eru eftir þörfum"

#: package/contents/ui/main.qml:154
#, kde-format
msgid "Toggle automatically loading this service on startup"
msgstr ""

#: package/contents/ui/main.qml:221
#, kde-format
msgid "Not running"
msgstr "Ekki keyrandi"

#: package/contents/ui/main.qml:222
#, kde-format
msgid "Running"
msgstr "Keyrandi"

#: package/contents/ui/main.qml:240
#, fuzzy, kde-format
#| msgid "Service"
msgid "Stop Service"
msgstr "Þjónusta"

#: package/contents/ui/main.qml:240
#, fuzzy, kde-format
#| msgid "Service"
msgid "Start Service"
msgstr "Þjónusta"

#~ msgid "kcmkded"
#~ msgstr "kcmkded"

#~ msgid "KDE Service Manager"
#~ msgstr "KDE Þjónustustjóri"

#~ msgid ""
#~ "This is a list of available KDE services which will be started on demand. "
#~ "They are only listed for convenience, as you cannot manipulate these "
#~ "services."
#~ msgstr ""
#~ "Þetta er listi af KDE þjónustum sem verða ræstar eftir þörfum. Þær eru "
#~ "aðeins listaðar hér þér til þæginda, þar sem þú getur ekki breytt þessum "
#~ "þjónustum."

#~ msgid "Status"
#~ msgstr "Staða"

#~ msgid "Description"
#~ msgstr "Lýsing"

#, fuzzy
#~| msgid ""
#~| "This shows all KDE services that can be loaded on KDE startup. Checked "
#~| "services will be invoked on next startup. Be careful with deactivation "
#~| "of unknown services."
#~ msgid ""
#~ "This shows all KDE services that can be loaded on Plasma startup. Checked "
#~ "services will be invoked on next startup. Be careful with deactivation of "
#~ "unknown services."
#~ msgstr ""
#~ "Þetta sýnir allar KDE þjónustur sem hægt er að ræsa þegar KDE er ræst. EF "
#~ "hakað er við þjónustu mun hún verða vakin við næstu ræsingu.  Verið "
#~ "varkár með að fjarlægja óþekktar þjónustur."

#~ msgid "Use"
#~ msgstr "Nota"

#~ msgid "Start"
#~ msgstr "Ræsa"

#~ msgid "Stop"
#~ msgstr "Stöðva"

#~ msgid "Unable to contact KDED."
#~ msgstr "Get ekki tengst KDED."

#~ msgid "Unable to start service <em>%1</em>.<br /><br /><i>Error: %2</i>"
#~ msgstr "Get ekki ræst þjónustuna <em>%1</em>.<br /><br /><i>Villa: %2</i>"

#~ msgid "Unable to stop service <em>%1</em>.<br /><br /><i>Error: %2</i>"
#~ msgstr ""
#~ "Get ekki stöðvað þjónustuna <em>%1</em>.<br /><br /><i>Villa: %2</i>"
